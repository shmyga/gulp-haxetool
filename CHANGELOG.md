0.1.6
-----
* Android: fix build result apk path

0.1.5
-----
* Haxe: default version 4.0.5

0.1.4
-----
* Config: android extensions params

0.1.3
-----
* Android: installed packages list
* Haxe: buildDir static property

0.1.2
-----
* Haxe: move build dir from tmp to project dir

0.1.1
-----
* Android sign apk (config.key.store and config.key.pass params)

0.1.0
------
* Android build
* Windows build
* Windows innosetup packer

0.0.18
------
* Add meta.fps project param

0.0.12
-----
* Openfl android platform support
* Android sdk module

0.0.11
------
* Added Neko module
* FlashPlayer output with stream
* Verbose output mode (--verbose)

0.0.10
------
* Windows compatibility
* Change FlashPlayer download link
* Use 'fs-extra' without 'mkdirp' and 'rmdir'
