[Setup]
AppName=<%=meta.title%>
AppVersion=<%=meta.version%>
WizardStyle=modern
DefaultDirName={autopf}\<%=meta.filename%>
DefaultGroupName=<%=meta.title%>
UninstallDisplayIcon={app}\<%=meta.filename%>.exe
Compression=lzma2
SolidCompression=yes
OutputDir=<%=output%>
OutputBaseFilename=<%=meta.filename%>_<%=meta.version%>

[Files]
Source: *; Excludes: "*.iss,*.log"; DestDir: "{app}"; Flags: recursesubdirs

[Icons]
Name: "{group}\<%=meta.title%>"; Filename: "{app}\<%=meta.filename%>.exe"
Name: "{group}\Uninstall <%=meta.title%>"; Filename: "{uninstallexe}"
