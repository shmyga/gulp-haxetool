class Swf {

    constructor(element, swf, options) {
        this.id = Math.floor(Math.random() * 10000);
        this.options = Object.assign({
            quality: "high",
            wMode: "opaque",
            width: "100%",
            height: "100%",
            vars: {},
        }, options);
        this.element = element;
        this.swf = swf;
        this.data = {
            params: {
                id: this.id,
                quality: this.options.quality,
                allowScriptAccess: "always",
                allowFullScreen: true,
                wMode: this.options.wMode,
                //base: base,
                swLiveConnect: true
            },
            properties: {
                name: this.id,
                width: this.options.width,
                height: this.options.height
            },
            vars: this.options.vars
        };
        this.swf = Swf.buildFlashElement(swf, this.data);
        this.element.appendChild(this.swf);
    }

    static toFlashVars(object, base) {
        var queryString = [];
        for (var key in object) {
            var value = object[key];
            if (base) key = base + ":" + key;
            var result;
            switch (typeof value) {
                case "object":
                    result = this.toFlashVars(value, key);
                    break;
                case "array":
                    var qs = {};
                    value.each(function (val, i) {
                        qs[i] = val;
                    });
                    result = this.toFlashVars(qs, key);
                    break;
                default:
                    result = key + "=" + encodeURIComponent(value);
            }
            if (value != null) queryString.push(result);
        }
        return queryString.join("&");
    }

    static buildFlashElement(path, options) {
        var params = options.params;
        var vars = options.vars;
        var properties = options.properties;

        params.flashVars = this.toFlashVars(vars);
        var isIE = /*@cc_on!@*/false;
        if (isIE) {
            properties.classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000";
            params.movie = path;
        } else {
            properties.type = "application/x-shockwave-flash";
        }
        properties.data = path;

        var build = "<object id=\"" + params.id + "\"";
        for (var property in properties) build += " " + property + "=\"" + properties[property] + "\"";
        build += ">";
        for (var param in params) {
            if (params[param]) build += "<param name=\"" + param + "\" value=\"" + params[param] + "\" />";
        }
        build += "</object>";

        var div = document.createElement("div");
        div.innerHTML = build;
        return div.firstChild;
    }
}
