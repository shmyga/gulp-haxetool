const fs = require('fs');
const fse = require('fs-extra');
const os = require('os');
const path = require('path');
const Sdk = require('./sdk');
const exec = require('./exec');
const template = require('lodash.template');

class InnoSetup extends Sdk {

    constructor(version) {
        super(InnoSetup.ID, version || InnoSetup.VERSION);
    }

    get link() {
        return 'http://www.jrsoftware.org/download.php/is.exe';
    }

    prepare(strip = 1) {
        let result = super.prepare(strip);
        if (!this.prepared) {
            result = result.then(() => {
                return exec(this.path, `is.exe /VERYSILENT /SUPPRESSMSGBOXES /DIR=${this.path}`);
            });
        }
        return result;
    }

    get isccBin() {
        return path.join(this.path, 'ISCC.exe')
    }

    get prepared() {
        try {
            return fs.existsSync(this.isccBin);
        } catch (e) {
            return false;
        }
    }

    pack(config, source, output) {
        const buildDir = path.join(os.tmpdir(), 'build', config.name, 'innosetup');
        const appTemplate = template(fs.readFileSync(path.resolve(__dirname, '..', 'template/windows/app.iss')));
        const app = appTemplate({...config, buildDir: buildDir, output: output});

        fse.ensureDirSync(buildDir);
        fse.copySync(`${source}`, `${buildDir}`);

        fs.writeFileSync(path.resolve(buildDir, 'app.iss'), app);
        return exec(buildDir, [`"${this.isccBin}"`, 'app.iss'].join(' '));
    }
}

InnoSetup.ID = 'innosetup';
InnoSetup.VERSION = '6';

module.exports = InnoSetup;
