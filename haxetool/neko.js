const os = require('os');
const fs = require('fs');
const path = require('path');
const run = require('../run/index');
const Sdk = require('./sdk');
const Env = require('./env');


class NekoLink {

    static get type() {
        switch (os.type()) {
            case 'Linux': return 'linux';
            case 'Windows_NT': return 'win';
        }
    }

    static get arch() {
        switch (os.arch()) {
            case 'ia32': return '';
            case 'x64': return '64';
        }
    }

    static get ext() {
        switch (os.type()) {
            case 'Linux': return 'tar.gz';
            case 'Windows_NT': return 'zip';
        }
    }

    static get(version) {
        const vv = version.split('\.').join('-');
        const filename = `neko-${version}-${NekoLink.type}${NekoLink.arch}.${NekoLink.ext}`;
        return `https://github.com/HaxeFoundation/neko/releases/download/v${vv}/${filename}`;
    }
}


class Neko extends Sdk {

    constructor(version) {
        super(Neko.ID, version || Neko.VERSION);
    }

    get link() {
        return NekoLink.get(this.version);
    }

    get prepared() {
        try {
            return fs.existsSync(this.nekoBin);
        } catch (e) {
            return false;
        }
    }

    activate() {
        Env.set('NEKO_INSTPATH', this.path);
        Env.addPath(this.path);
        Env.addPath(this.path, 'LD_LIBRARY_PATH');
    }

    prepare() {
        return Promise.all([super.prepare()]).then(() => {
            fs.copyFileSync(path.resolve(__dirname, '..', 'template/neko/activate'), path.resolve(this.path, 'activate'));
        });
    }

    get nekoBin() {
        let binname = 'neko';
        if (os.type() === 'Windows_NT') binname += '.exe';
        return path.join(this.path, binname);
    }

    run(filename, params) {
        this.log.i('_run_ *%s*', filename);
        this.activate();
        return run(`${this.nekoBin} ${filename}`, params).exec();
    }
}

Neko.ID = 'neko';

Neko.VERSION_2_2_0 = '2.2.0';
Neko.VERSION = Neko.VERSION_2_2_0;

module.exports = Neko;
