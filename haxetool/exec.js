const child_process = require('child_process');
const async = require('async');
const log = require('./log')('exec');


const queue = async.queue((task, done) => {
    log.d('*%s*', task.command);
    //process.chdir(task.dir);
    child_process.exec(task.command, {cwd: task.dir, maxBuffer: 1024 * 10000}, (err, stdout, stderr) => {
        if (err) {
            log.v('!%s!', err);
            task.failure('\n' + (stderr || stdout || err));
        } else {
            log.v('%s', stdout);
            task.success({stdout: stdout, stderr: stderr});
        }
        done();
    });
});

module.exports = (dir, command) => {
    return new Promise((success, failure) => {
        queue.push({
            dir: dir,
            command: command,
            success: success,
            failure: failure,
        });
    });
};
