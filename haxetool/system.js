const os = require('os');

class System {

    static get isWindows() {
        return os.type() === 'Windows_NT';
    }

    static get isLinux() {
        return os.type() === 'Linux';
    }

    static get os() {
        return (
            this.isWindows ? 'windows' :
            this.isLinux ? 'linux' :
                undefined
        );
    }

    static get archInt() {
        if (os.arch() === 'ia32') return 32;
        if (os.arch() === 'x64') return 64;
    }

    static get isArch32() {
        return this.archInt === 32;
    }

    static get isArch64() {
        return this.archInt === 64;
    }
}

module.exports = System;
