const path = require('path');


class Env {

    static set(key, value) {
        process.env[key] = value;
    }

    static get(key) {
        return process.env[key];
    }

    static addPath(value, key='PATH') {
        if (!process.env[key]) {
            process.env[key] = value;
        } else if (process.env[key].split(path.delimiter).indexOf(value) === -1) {
            process.env[key] = [process.env[key], value].join(path.delimiter);
        }
    }
}


module.exports = Env;
