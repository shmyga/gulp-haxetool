const os = require('os');
const path = require('path');


const BuildSystem = {
    HAXE: 'haxe',
    OPENFL: 'openfl',
    ADOBE_AIR: 'adobe_air',
};


const Platform = {
    FLASH: 'flash',
    HTML5: 'html5',
    LINUX: 'linux',
    WINDOWS: 'windows',
    ANDROID: 'android',
    NEKO: 'neko',
    CPP: 'cpp',
};


class Config {

    constructor(params) {
        this._params = [];
        this.name = null;
        this.main = null;
        this.preloader = null;
        this.sources = [];
        this.assets = [];
        this.libs = [];
        this.macros = [];
        this.flags = [];
        this.icon = null;
        this.meta = {
            title: null,
            filename: null,
            icon: null,
            version: null,
            pack: null,
            author: null,
            company: null,
            width: 800,
            height: 600,
            mobileWidth: null,
            mobileHeight: null,
            fps: 60,
        };
        this.key = null;
        this.android = [];
        if (params) {
            this.update(params);
            this.afterUpdate();
        }
    }

    static absolutePath(file) {
        const result = path.resolve(process.cwd(), path.normalize(file));
        return os.type() === 'Windows_NT' ? result.split('\\').join('/') : result;
    }

    update(params) {
        this._params.push(params);
        if (params.name !== undefined) this.name = params.name;
        if (params.main !== undefined) this.main = params.main;
        if (params.preloader !== undefined) this.preloader = params.preloader;
        if (params.sources !== undefined) this.sources = this.sources.concat(params.sources.map(Config.absolutePath));
        if (params.assets !== undefined) this.assets = this.assets.concat(params.assets.map(Config.absolutePath));
        if (params.libs !== undefined) this.libs = this.libs.concat(Array.isArray(params.libs) ? params.libs : Object.entries(params.libs).map(([k, v]) => ({name: k, version: v})));
        if (params.macros !== undefined) this.macros = this.macros.concat(params.macros);
        if (params.flags !== undefined) this.flags = this.flags.concat(params.flags);
        if (params.meta !== undefined) this.meta = {...this.meta, ...params.meta};
        if (params.key !== undefined) this.key = params.key;
        if (params.android !== undefined) this.android = this.android.concat(params.android.map(item => ({path: Config.absolutePath(item.path), extensions: item.extensions})));
        if (this.meta.icon) this.icon = Config.absolutePath(this.meta.icon);
    }

    afterUpdate() {
        if (this.meta.mobileWidth === null) this.meta.mobileWidth = this.meta.width;
        if (this.meta.mobileHeight === null) this.meta.mobileHeight = Math.round(this.meta.mobileWidth / 1.777777778);
    }

    branch(params) {
        const result = new Config();
        for (const params of this._params) {
            result.update(params);
        }
        result.update(params);
        result.afterUpdate();
        return result;
    }
}


module.exports = {
    BuildSystem: BuildSystem,
    Platform: Platform,
    Config: Config,
};
