const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const os = require('os');
const through = require('through2');
const Sdk = require('./sdk');
const System = require('./system');
const run = require('../run/index');
const {TailVinyl} = require('./tail');


class FlashPlayer extends Sdk {

    constructor(debug, version) {
        super(FlashPlayer.ID, version || FlashPlayer.VERSION);
        this.debug = debug;
    }

    prepare() {
        return super.prepare(0)
    }

    get prepared() {
        try {
            return fs.existsSync(this.flashPlayerBin);
        } catch (e) {
            return false;
        }
    }

    get link() {
        const baseUrl = `https://fpdownload.macromedia.com/pub/flashplayer/updaters/${this.version}/`;
        if (System.isWindows) {
            return baseUrl + `flashplayer_${this.version}_sa${this.debug ? '_debug' : ''}.exe`;
        } else if (System.isLinux) {
            return baseUrl + `flash_player_sa_linux${this.debug ? '_debug' : ''}.x86_64.tar.gz`;
        } else {
            throw `Unsupported os '${os.type()}'`;
        }
    }

    get flashPlayerBin() {
        if (os.type() === 'Windows_NT') {
            const v = this.version.split('.');
            const playerName = `flashplayer_${this.version}_sa${this.debug ? '_debug' : ''}.exe`;
            return path.join(this.path, playerName);
        } else if (os.type() === 'Linux') {
            const binPath = path.join(this.path, `flashplayer${this.debug ? 'debugger' : ''}`);
            fs.chmodSync(binPath, 0o755);
            return binPath;
        } else {
            throw `Unsupported os '${os.type()}'`;
        }
    }

    static get flashPlayerDir() {
        if (os.type() === 'Windows_NT') {
            return path.join(process.env.APPDATA, 'Macromedia', 'Flash Player');
        } else if (os.type() === 'Linux') {
            return path.join(os.homedir(), '.macromedia', 'Flash_Player');
        }
    }

    static get log() {
        return path.join(this.flashPlayerDir, 'Logs', 'flashlog.txt');
    }

    static enableLog() {
        const filename = path.join(os.homedir(), 'mm.cfg');
        const value = 'TraceOutputFileEnable=1';
        if (fs.existsSync(filename)) {
            const data = fs.readFileSync(filename);
            if (data.indexOf(value) === -1) {
                fs.appendFileSync(filename, `${value}\n`);
            }
        } else {
            fs.writeFileSync(filename, `${value}\n`);
        }
        fse.ensureDirSync(path.join(this.flashPlayerDir, 'Logs'));
        fs.writeFileSync(FlashPlayer.log, '');
    }

    static trust(value) {
        const filename = path.join(this.flashPlayerDir, '#Security', 'FlashPlayerTrust', 'gulp.cfg');
        if (fs.existsSync(filename)) {
            const data = fs.readFileSync(filename);
            if (data.indexOf(value) === -1) {
                fs.appendFileSync(filename, `${value}\n`);
            }
        } else {
            if (!fs.existsSync(path.dirname(filename))) {
                fse.ensureDirSync(path.dirname(filename));
            }
            fs.writeFileSync(filename, `${value}\n`);
        }
    }

    run(filename, params) {
        this.log.i('_run_ *%s*', filename);
        FlashPlayer.trust(filename);
        FlashPlayer.enableLog();
        return run(`${this.flashPlayerBin} ${filename}`, params).exec()
            .pipe(through.obj(function (file, enc, callback) {
                const log = new TailVinyl({
                    path: FlashPlayer.log,
                });
                this.on('end', () => log.dispose());
                this.push(log);
                callback();
            }));
    }
}

FlashPlayer.ID = 'flashplayer';

FlashPlayer.VERSION_24 = '24';
FlashPlayer.VERSION_25 = '25';
FlashPlayer.VERSION_26 = '26';
FlashPlayer.VERSION_27 = '27';
FlashPlayer.VERSION_28 = '28';
FlashPlayer.VERSION_29 = '29';
FlashPlayer.VERSION_30 = '30';
FlashPlayer.VERSION_31 = '31';
FlashPlayer.VERSION_32 = '32';

FlashPlayer.VERSION = FlashPlayer.VERSION_32;

module.exports = FlashPlayer;
