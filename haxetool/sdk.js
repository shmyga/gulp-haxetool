const os = require('os');
const path = require('path');
const fs = require('fs');
const ps = require('promise-streams');
const got = require('got');
const unzip = require('unzip-stream');
const tar = require('tar');
const progress = require('cli-progress');
const fse = require('fs-extra');
const Log = require('./log');

class Downloader {

    static createProgressBar() {
        if (Downloader.bar == null) {
            Downloader.bar = new progress.MultiBar({
                clearOnComplete: true,
            }, progress.Presets.shades_grey);
        }
        return Downloader.bar.create();
    }

    static releaseProgressBar(bar) {
        bar.stop();
        if (Downloader.bar != null) {
            Downloader.bar.remove(bar);
            if (Downloader.bar.bars.length === 0) {
                Downloader.bar.stop();
                Downloader.bar = null;
            }
        }
    }

    static download(url, dest, strip = 1, executable = false) {
        fse.ensureDirSync(dest);
        const bar = Downloader.createProgressBar();
        let stream = got.stream(url);
        stream = stream
            .on('request', r => {
                if (bar) bar.start(1, 0);
            })
            .on('downloadProgress', p => {
                if (bar) {
                    bar.setTotal(p.total);
                    bar.update(p.transferred);
                    if (p.total === p.transferred) {
                        Downloader.releaseProgressBar(bar);
                    }
                }
            });
        if (url.endsWith('.zip')) {
            stream = stream.pipe(unzip.Parse()).on('entry', entry => {
                const filePath = entry.path.split('/').slice(strip).join(path.sep);
                if (filePath.length > 0) {
                    if (entry.type === 'Directory') {
                        fse.ensureDirSync(path.join(dest, path.normalize(filePath)));
                    } else if (entry.type === 'File') {
                        const fullFilePath = path.join(dest, path.normalize(filePath));
                        const dirPath = path.dirname(fullFilePath);
                        if (!fs.existsSync(dirPath)) {
                            fse.ensureDirSync(dirPath);
                        }
                        entry.pipe(fs.createWriteStream(path.join(dest, path.normalize(filePath)), {
                            // ToDo: zip entry file mode?
                            mode: executable ? 0o755 : undefined,
                        }));
                    }
                } else {
                    entry.autodrain();
                }
            });
        } else if (url.endsWith('tar.gz')) {
            stream = stream.pipe(tar.x({C: dest, strip: strip}));
        } else {
            stream = stream.pipe(fs.createWriteStream(path.join(dest, url.split('/').pop())));
        }
        return ps.wait(stream);
    }
}


class Sdk {
    static set dir(value) {
        Sdk._dir = value
    }

    static get dir() {
        return Sdk._dir || path.join(os.homedir(), 'sdk');
    }

    static path(name, version) {
        return path.join(this.dir, name, version);
    }

    constructor(name, version) {
        this.name = name;
        this.log = Log(name);
        this.version = version;
        this.path = Sdk.path(name, version);
    }

    get intVersion() {
        const vArr = this.version.split('\.').reverse();
        let m = 1;
        let r = 0;
        for (let v of vArr) {
            r += parseInt(v) * m;
            m *= 10;
        }
        return r;
    }

    get prepared() {
        throw "Not implemented";
    }

    get link() {
        throw "Not implemented";
    }

    prepare(strip = 1) {
        this.log.i('version: *%s*', this.version);
        if (this.prepared) {
            return Promise.resolve();
        } else {
            this.log.d('download: *%s*', this.link);
            return Downloader.download(this.link, this.path, strip);
        }
    }
}

Sdk.Downloader = Downloader;

module.exports = Sdk;
