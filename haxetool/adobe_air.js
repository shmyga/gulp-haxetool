const fs = require('fs');
const tmp = require('tmp-file');
const exec = require('./exec');
const gulp = require('gulp');
const through = require('through2');
//const mark = require('gulp-mark');
//const replace = require('gulp-replace-task');
const Sdk = require('./sdk');


// ToDo: remove gutil
class AdobeAir extends Sdk {

    get adtBin() {
        return `${this.path}/bin/adt.bat`;
    }

    get adlBin() {
        return `${this.path}/bin/adl.exe`;
    }

    static buildAdtArgs(params, descriptor, keystore, files, output) {
        //const quote = (value) => `"${value}"`;
        const quote = (value) => value;  // ToDo:
        const command = ['-package'];
        const target = params.target || 'native';
        if (target !== 'native') command.push('-target', target);  //ToDo: adobe bleat' (target param position in native and apk-captive-runtime targets)
        if (params.useLegacyAOT) {
            command.push('-useLegacyAOT', 'yes');
        }
        if (params.profile) {
            command.push('-provisioning-profile', quote(params.profile));
        }
        if (keystore) {
            command.push('-storetype', 'PKCS12');
            command.push('-keystore', keystore.path);
            command.push('-storepass', keystore.storepass);
        }
        if (params.target === 'native') {
            command.push('-tsa', 'http://sha256timestamp.ws.symantec.com/sha256/timestamp');
            command.push('-target', target);
        }
        command.push(quote(output), quote(descriptor.path));
        if (params.extdir) {
            command.push('-extdir', quote(params.extdir));
        }
        if (params.content) {
            for (let k in params.content) if (params.content.hasOwnProperty(k)) {
                command.push('-C', quote(k), quote(params.content[k]));
            }
        }
        for (let file of files) {
            command.push('-C', quote(file.base), quote(file.path.split(file.base)[1]));
        }
        return command;
    };

    constructor(version) {
        super(AdobeAir.ID, version || AdobeAir.VERSION);
    }

    get prepared() {
        return fs.existsSync(`${this.path}/air-sdk-description.xml`);
    }

    get link() {
        return `http://airdownload.adobe.com/air/win/download/${this.version}/AIRSDK_Compiler.zip`;
    }

    adt(path, args) {
        return exec(path, [this.adtBin].concat(args).join(' '));
    }

    adl(path, args) {
        return exec(path, [this.adlBin].concat(args).join(' '));
    }

    pack(params) {
        const files = [];
        let descriptor = null;
        let keystore = null;
        let stream = null;

        const bufferContents = (file, enc, callback) => {
            // ToDo: check file not stream
            switch (file.mark) {
                case 'descriptor':
                    descriptor = file;
                    break;
                case 'keystore':
                    keystore = file;
                    break;
                default:
                    files.push(file);
            }
            callback();
        };

        const endStream = (callback) => {
            gutil.log(this.tag, col.cyan('adt', 'build', params.target), '=>', col.magenta(params.outputFile));
            if (!descriptor) {
                stream.emit('error', new gutil.PluginError({plugin: this.name, message: 'descriptor is not defined'}));
                callback();
                return;
            }
            const tmpFile = tmp.generateFile();
            const command = AdobeAir.buildAdtArgs(params, descriptor, keystore, files, tmpFile.path);
            this.adt('.', command)
                .then(() => {
                    stream.push(new gutil.File({
                        path: params.outputFile,
                        contents: fs.createReadStream(tmpFile.path)
                    }));
                    callback();
                })
                .catch((error) => {
                    stream.emit('error', new gutil.PluginError({plugin: this.name, message: error}));
                    callback();
                });
        };

        return stream = through.obj(bufferContents, endStream);
    }

    install() {
        let stream = null;
        const bufferContents = (file, enc, callback) => {
            gutil.log(this.tag, col.cyan('adt', 'install'), col.magenta(file.name));
            const command = ['-installApp', '-platform', 'android', '-package', file.path];
            this.adt('.', command)
                .then(() => {
                    stream.push(file);
                    callback();
                })
                .catch((error) => {
                    stream.emit('error', new gutil.PluginError({plugin: this.name, message: error}));
                    callback();
                });
        };
        return stream = through.obj(bufferContents);
    }

    launch(appid) {
        let stream = null;
        const bufferContents = (file, enc, callback) => {
            gutil.log(this.tag, col.cyan('adt', 'launch'), col.magenta(appid));
            const command = ['-launchApp', '-platform', 'android', '-appid', appid];
            this.adt('.', command)
                .then(() => {
                    stream.push(file);
                    callback();
                })
                .catch((error) => {
                    stream.emit('error', new gutil.PluginError({plugin: this.name, message: error}));
                    callback();
                });
        };
        return stream = through.obj(bufferContents);
    }

    test(params) {
        let root = null;
        let descriptor = null;
        let stream = null;

        const bufferContents = (file, enc, callback) => {
            // ToDo: check file not stream
            switch (file.mark) {
                case 'descriptor':
                    descriptor = file;
                    break;
                default:
                    root = file;
            }
            callback();
        };

        const endStream = (callback) => {
            gutil.log(this.tag, col.cyan('adl'));
            //const command = AdobeAir.buildAdtArgs(params, files, tmpFile.path);
            const command = [];
            if (params.profile) command.push('-profile', params.profile);
            if (params.extdir) command.push('-extdir', params.extdir);
            if (!descriptor) {
                stream.emit('error', new gutil.PluginError({plugin: this.name, message: 'descriptor is not defined'}));
                callback();
                return;
            }
            command.push(descriptor.path);
            command.push(root.path);
            if (params.args) {
                command.push('--');
                for (let key of Object.keys(params.args)) {
                    const value = params.args[key];
                    if (value === true) {
                        command.push(`-${key}`);
                    } else if (value) {
                        command.push(`-${key}`, `${value}`);
                    }
                }
            }
            this.adl('.', command)
                .then(() => {
                    stream.emit('end');
                    callback();
                })
                .catch((error) => {
                    stream.emit('error', new gutil.PluginError({plugin: this.name, message: error}));
                    callback();
                });

            //callback();
            stream.push(root);
        };

        return stream = through.obj(bufferContents, endStream);
    }

    static descriptor(template, values) {
        const tmpFile = tmp.generateFile();
        const patterns = [];
        for (let k in values) if (values.hasOwnProperty(k)) {
            patterns.push({match: k, replacement: values[k]});
        }
        return gulp.src(template)
            .pipe(replace({patterns: patterns}))
            .pipe(mark.set('descriptor'))
            .pipe(gulp.dest(tmpFile.path));
    }

    static keystore(keystore, storepass) {
        return gulp.src(keystore).pipe(through.obj((file, enc, callback) => {
            file.mark = 'keystore';
            file.storepass = storepass;
            callback(null, file);
        }));
    }

    static pack(params) {
        return new AdobeAir().pack(params);
    }

    static test(params) {
        return new AdobeAir().test(params);
    }
}

AdobeAir.ID = 'adobe-air';

AdobeAir.VERSION_26 = '26.0';
AdobeAir.VERSION_25 = '25.0';
AdobeAir.VERSION_24 = '24.0';
AdobeAir.VERSION = AdobeAir.VERSION_26;

module.exports = AdobeAir;