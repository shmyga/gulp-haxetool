const colors = require('ansi-colors');
const timestamp = require('time-stamp');


const Level = {
  VERBOSE: -1,
  DEBUG: 0,
  INFO: 1,
  WARN: 2,
  ERROR: 3,
};

const level = process.argv.indexOf('--verbose') > -1 ? Level.VERBOSE : Level.INFO;


class Log {

    static colorize(pattern) {
        for (const item of [
            ['_', colors.cyan],
            ['\\*', colors.magenta],
            ['!', colors.red],
        ]) {
            const r = new RegExp(item[0] + "(.*?)" + item[0], 'g');
            pattern = pattern.replace(r, item[1]('$1'));
        }
        return pattern
    }

    static write(level, tag, pattern, ...args) {
        if (level >= this.level) {
            const message = `[${colors.gray(timestamp('HH:mm:ss'))}] ${colors.green(tag)} ${this.colorize(pattern)}`;
            console.log(message, ...args)
        }
    }

    constructor(tag) {
        this.tag = tag;
    }

    v(...args) {
        Log.write(-1, this.tag, ...args);
    }

    d(...args) {
        Log.write(0, this.tag, ...args);
    }

    i(...args) {
        Log.write(1, this.tag, ...args);
    }

    w(...args) {
        Log.write(2, this.tag, ...args);
    }

    e(...args) {
        Log.write(3, this.tag, ...args);
    }
}

Log.level = level;

module.exports = (tag) => new Log(tag);
