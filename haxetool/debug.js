const colors = require('ansi-colors');


const _colors = {
    '[DEBUG]': colors.white,
    '[INFO]': colors.cyan,
    '[ERROR]': colors.red,
    'Called from ': colors.red,
    '[WARNING]': colors.yellow,
};

const getColor = (line) => {
    for (const [tag, color] of Object.entries(_colors)) {
        if (line.indexOf(tag) > -1) {
            return color;
        }
    }
    return null;// colors.reset;
};


class Debug {

    constructor() {
        this.color = colors.white;
        this.log = this.log.bind(this);
    }

    log(line) {
        const newColor = getColor(line);
        if (newColor) this.color = newColor;
        if (line[0] === '\t' || line.startsWith('Called from ')) {
            console.log(this.color(line));
        } else {
            const result = line.split(' ');
            console.log(colors.gray(result.slice(0, 4).join(' ')) + ' ' + this.color(result.slice(4).join(' ')));
        }
    }
}

module.exports = new Debug();
