module.exports = {
    System: require('./haxetool/system'),
    Sdk: require('./haxetool/sdk'),
    Haxe: require('./haxetool/haxe'),
    FlashPlayer: require('./haxetool/flashplayer'),
    Android: require('./haxetool/android'),
    AdobeAir: require('./haxetool/adobe_air'),
    Project: require('./haxetool/project'),
};
