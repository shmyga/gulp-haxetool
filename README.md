# Gulp HaXe Tool

## Introducing

### Dignity

* Automatically download HaXe SDK, Neko, FlashPlayer, Android SDK, +(Adobe AIR SDK).
* Universal project config for simple HaXe build and OpenFL build.
* Branch project config for many build presets (many OpenFL apps in one project).
* Packaging linux output in DEB.
* Packaging windows output in exe install with InnoSetup.
* Create html wrapper for flash output.

### Flaws

* Hardcode build output in {projectDir}/target (need param in Project.Config).
* HTML5 run output not caught.
* Adobe AIR Build System in progress.
* Windows target depends Visual Studio 2017 build tools

## API

### Project.Config

#### constructor

* **meta**: meta information 
    * **title**: application title
    * **filename**: application filename
    * **icon**: path to icon file
    * **pack**: package name for OpenFL
    * **author**: author name for DEB packaging
    * **company**: company name for OpenFL
    * **version**: application version
* **name**: name for output directory
* **libs**: list of haxe dependencies
* **sources**: list of HaXe sources
* **assets**: list of resources
* **main**: main class name
* **macros**: list of macros

### Project

#### constructor

* **buildSystem**: Project.BuildSystem
* **platforms**: Project.Platform (list supported)
* **config**: Project.Config

#### bind

* **module**

create gulp tasks in module:

```
<config.name>:<platform>:build
<config.name>:<platform>:run
<config.name>:<platform>:test

<config.name>:flash:html

<config.name>:linux:archive 
<config.name>:linux:deb 

<config.name>:windows:archive 
<config.name>:windows:installer 
```

eg: ```gulp app:flash:test```

## Example

[package.json](example/package.json)
```json
{
    "name": "app",
    "version": "0.0.1",
    "devDependencies": {
        "gulp": "^4.0.0",
        "gulp-clean": "^0.4.0",
        "gulp-haxetool": "^0.1.0"
    },
    "haxeDependencies": {
        "lime": "7.6.0",
        "openfl": "8.9.2",
        "hxcpp": "4.0.19"
    }
}
```

[src/Main.hx](example/src/Main.hx)
```haxe
import flash.display.Shape;
import flash.Lib;

class Main {

    public static function main() {
        trace("Hello world!");
        var shape = new Shape();
        shape.graphics.beginFill(0x00ff00);
        shape.graphics.drawCircle(50, 50, 20);
        shape.graphics.endFill();
        Lib.current.addChild(shape);
    }
}
```

[gulpfile.js](example/gulpfile.js)
```javascript
const gulp = require('gulp');
const gulpClean = require('gulp-clean');
const packageInfo = require('./package.json');
const {System, Sdk, Haxe, Project} = require('gulp-haxetool');

//Sdk.dir = '/opt/sdk';

exports.clean = function clean() {
    return gulp.src('target/*', {read: false}).pipe(gulpClean());
};

exports.install = function install() {
    const haxe = new Haxe();
    return haxe.prepare().then(() => haxe.install(packageInfo.haxeDependencies));
};

const config = new Project.Config({
    meta: {
        title: 'Application',
        filename: 'app',
        icon: './resources/icon.jpg',
        pack: 'com.example',
        author: 'Anonim <anonim@anonim.com>',
        company: 'Any company',
        version: packageInfo.version,
    },
    name: 'app',
    libs: packageInfo.haxeDependencies,
    sources: ['src'],
    assets: ['resources'],
    main: 'Main',
});

const project = new Project(
    Project.BuildSystem.OPENFL,
    [
        Project.Platform.FLASH,
        Project.Platform.HTML5,
        Project.Platform.LINUX,
        Project.Platform.WINDOWS,
    ],
    config
).bind(module);


const defaultSeries = [
    exports.clean,
    module.exports['app:flash:build'],
    module.exports['app:flash:html'],
    module.exports['app:html5:build'],
];

if (System.isLinux) {
    defaultSeries.push(
        module.exports['app:linux:build'],
        module.exports['app:linux:archive'],
        module.exports['app:linux:deb'],

        module.exports['app:android:build'],
    );
}

if (System.isWindows) {
    defaultSeries.push(
        module.exports['app:windows:build'],
        module.exports['app:windows:archive'],
        module.exports['app:windows:installer'],
    );
}

module.exports.default = gulp.series(defaultSeries);
```

Build for all platforms: `gulp default`

Build app for specific platform: `gulp app:flash:build`

Build and run flash app in flashplayer: `gulp app:flash:test`

Build and run html5 app in browser: `gulp app:html5:test`

Build and run linux app native: `gulp app:linux:test`

Build and run windows app native: `gulp app:windows:test`

Build and run android app: `gulp app:android:test`
