const gulp = require('gulp');
const gulpClean = require('gulp-clean');
const packageInfo = require('./package.json');
const {System, Sdk, Haxe, Project} = require('gulp-haxetool');

//Sdk.dir = '/opt/sdk';

exports.clean = function clean() {
    return gulp.src('target/*', {read: false}).pipe(gulpClean());
};

exports.install = function install() {
    const haxe = new Haxe();
    return haxe.prepare().then(() => haxe.install(packageInfo.haxeDependencies));
};

const config = new Project.Config({
    meta: {
        title: 'Application',
        filename: 'app',
        icon: './resources/icon.jpg',
        pack: 'com.example',
        author: 'Anonim <anonim@anonim.com>',
        company: 'Any company',
        version: packageInfo.version,
    },
    name: 'app',
    libs: packageInfo.haxeDependencies,
    sources: ['src'],
    assets: ['resources'],
    main: 'Main',
});

const project = new Project(
    Project.BuildSystem.OPENFL,
    [
        Project.Platform.FLASH,
        Project.Platform.HTML5,
        Project.Platform.LINUX,
        Project.Platform.WINDOWS,
    ],
    config
).bind(module);


const defaultSeries = [
    exports.clean,
    module.exports['app:flash:build'],
    module.exports['app:flash:html'],
    module.exports['app:html5:build'],
];

if (System.isLinux) {
    defaultSeries.push(
        module.exports['app:linux:build'],
        module.exports['app:linux:archive'],
        module.exports['app:linux:deb'],

        module.exports['app:android:build'],
    );
}

if (System.isWindows) {
    defaultSeries.push(
        module.exports['app:windows:build'],
        module.exports['app:windows:archive'],
        module.exports['app:windows:installer'],
    );
}

module.exports.default = gulp.series(defaultSeries);
