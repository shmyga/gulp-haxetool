import flash.display.Shape;
import flash.Lib;

class Main {

    public static function main() {
        trace("Hello world!");
        var shape = new Shape();
        shape.graphics.beginFill(0x00ff00);
        shape.graphics.drawCircle(50, 50, 20);
        shape.graphics.endFill();
        Lib.current.addChild(shape);
    }
}